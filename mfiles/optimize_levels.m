
function [probs, cap] = optimize_levels(nlevels, noise, dimming, type)
%nLevels -> quantidade de níveis que serão igualmente distribuídos entre 0
%           e 1
%noise     -> nível do ruído, considere que o sinal variam de 0 até 1
%             assim teremos que a relação sinal ruído será de 
%             10log10(1/noise)
%dimming   -> fator de dimming utilizado
%
% probs    -> probabilidades depois de otimizado.
% cap      -> capacidade resultante da otimização

    count = 0;

    fun1 = @(x)(calcCap1(x, nlevels, noise, type));
    
    %%%%%%%%%%%%%%%%%%%
    % Garante que a soma das probabilidades seja 1
    % E que o dimming seja respeitado
    Aeq1 = [ones(1,nlevels) ; 
        (0:(nlevels-1))/(nlevels-1)
        ];
    beq1 = [1 ; dimming];
    %Ax<=b
    A1=[ ];
    b1= [];
    
    %%%%%%%%%%%%%%%%%%%%%
    % Limites superiores e inferiores
    lb = zeros(1,nlevels);
    ub = ones(1,nlevels);
    
    %%%%%%%%
    % Valor inicial
    x_ini = ones(1, nlevels)/nlevels;
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Opções da otimização
    opts = optimoptions('fmincon','TolFun', 1e-11, 'TolCon', 1e-10,...
     'Display', 'off', 'Algorithm', 'sqp');
 
    %%%%%%%%%%%%%%%%
    % funcNiv
    funcNiv = [];
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Otimização
    [probs, cap]=fmincon(fun1, x_ini, A1, b1, ...
        Aeq1, beq1, lb, ub, funcNiv, ...
        opts);
    
    cap = -cap;

end

function cap=calcCap1(x, nlevels, noise, type)
persistent count;
if(length(count)==0) 
    count=0;
end ;
count=count+1;
levels=(0:(nlevels-1))/(nlevels-1);
if(type == 0)
    cap = -vlccapteste(levels, x, noise);
else
    cap = -capacity_vlc2(levels, x, noise);
end
if(rem(count, 100)==0)
    
    disp(x)
    disp(sprintf('cap=%f - c=%d', cap, count));
end
end

