

%Consideramos a potência óptica como P/sigma (noise=sigma)
dimming = 0.2;
nlevels=40;
nValues = 12;
iniValue=5;
fimValue = 10;

values = (0:(nValues-1))/(nValues-1);
values = values * (fimValue-iniValue)+iniValue;

probf = zeros(nValues, nlevels);
capf = zeros(nValues,1);
noises = zeros(nValues,1);
ind=1;

for vdb=values
    noise = 10^(-vdb/10);
    
    [probs, cap] = optimize_levels(nlevels, noise, dimming, 1);
    probf(ind,:)=probs;
    capf(ind)=cap;
    ind = ind+1;
end

x = values'*ones(1,nlevels);
y = ones(nValues, 1)*(0:(nlevels-1))/(nlevels-1);


x=x(:);
y=y(:);
z=probf(:);
ff=1;
for i=1:length(x)
    if (z(ff) < 2e-3)
        z(ff)=[];
        x(ff)=[];
        y(ff)=[];
    else
        ff=ff+1;
    end
    
end

figure()

stem3(x,y,z);

figure()

plot(values, capf);

