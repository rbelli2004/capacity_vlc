/*
 * capacity_vlc.cpp
 *
 *  Created on: 16 de out de 2016
 *      Author: belli
 */



#define PI 3.14159265359

#include "mex.h"
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_message.h>
#include <gsl/gsl_integration.h>
#include <iostream>

struct ParamsFunc
{
	double *niveis;
	double *probs;
	int m;
	double sig;
	double sig2;
};

static double calcInfLog(double y, ParamsFunc *params)
{
	double res = 0;
	int m = params->m;
	for(int i=0;i<m;++i)
	{
		double temp = y-params->niveis[i];
		res+=params->probs[i]*exp(-temp*temp/params->sig2);
	}
	return res;
}

static double calcLog2val(double y, ParamsFunc *params, int j)
{
	double temp = y-params->niveis[j];
	double divisor = calcInfLog(y, params);
	double mult = exp(-temp*temp/(params->sig2));

	double v1 = mult / divisor;
	if(v1 < 1e-100)
	{
		return 0.0;
	}
	return log2(v1);
}

static double calcLog2val_2(double y,  ParamsFunc *params, int j)
{
	double temp1 = y-params->niveis[j];
	temp1=temp1*temp1;
	int m=params->m;
	double res=0;
	for(int i=0;i<m;++i)
	{
		if(i==j)
		{
			res+=params->probs[i];
		}
		else
		{
			double temp = y-params->niveis[i];
			temp=temp*temp;
			res+=params->probs[i]*exp((temp1-temp)/params->sig2);

		}
	}
	//mexPrintf("res=%e ", res);

	return -log2(res);
}

void errorgslhandler (const char * reason,
              const char * file,
              int line,
              int gsl_errno)
{
	char temp[5000];
	sprintf(temp, "Erro %s - %s - %d - %d\n", reason, file, line, gsl_errno);
	mexErrMsgTxt(temp);
}



static double calc_integrand(double y, void *params_int)
{
	ParamsFunc *params = (ParamsFunc *)params_int;

	int m = params->m;
	double res = 0;
	for(int i=0;i<m;++i)
	{
		double temp = y-params->niveis[i];
		temp=temp*temp/params->sig2;
		if(temp > 100)
		{
			continue;
		}
		double res1 = calcLog2val_2(y, params, i);
		//mexPrintf("  - res %e temp*temp=%e\n", res1, temp*temp/params->sig2);
		res += params->probs[i] * exp(-temp) * res1;
	}
	return res;
}

void calcMaxMin(double *levels, int nLevels, double *maxv, double *minv)
{
	*maxv = 0;
	*minv = 1;
	for(int i=0;i<nLevels;++i)
	{
		if(*maxv<levels[i])
		{
			*maxv=levels[i];
		}
		if(*minv>levels[i])
		{
			*minv=levels[i];
		}
	}
}


/**
 * capacity_vlc(levels, probs, sig)
 */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
	if(nrhs != 3 || (nlhs != 1 && nlhs !=0))
	{
		mexErrMsgTxt("Wrong number of parameters: answer = capacity_vlc(levels, probs, sig);");
	}

	int nLevels = mxGetM(prhs[0])*mxGetN(prhs[0]);
	int nProbs = mxGetM(prhs[1])*mxGetN(prhs[1]);
	if(nLevels != nProbs)
	{
		mexErrMsgTxt("Erro, tamanho diferente entre levels e probs: capacity_vlc(levels, probs, sig);");
	}

	double *levels, *probs, *sig;
	levels= mxGetPr(prhs[0]);
	probs = mxGetPr(prhs[1]);
	sig   = mxGetPr(prhs[2]);

	ParamsFunc params;
	params.m = nLevels;
	params.niveis = levels;
	params.probs  = probs;
	params.sig    = sig[0];
	params.sig2   = params.sig * params.sig * 2;

	//mexPrintf("m=%d - sig=%f\n", params.m, params.sig);


	gsl_integration_workspace *w = gsl_integration_workspace_alloc (2000);
	gsl_function func1;
	func1.function = &calc_integrand;

	func1.params = (void *)&params;
	double result;
	double error;
	double maxv, minv;
	calcMaxMin(levels, nLevels, &maxv, &minv);

	//mexPrintf("maxv=%f minv=%f\n", maxv, minv);
	gsl_set_error_handler (errorgslhandler);

	gsl_integration_qags (&func1, minv-8*sig[0], maxv+8*sig[0], 1e-6, 1e-6, 2000,
						w, &result, &error);

	result = result/sqrt(params.sig2*PI);

	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
	double *res = mxGetPr(plhs[0]);
	res[0]=0;


}
