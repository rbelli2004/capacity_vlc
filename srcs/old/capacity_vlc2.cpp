/*
 * capacity_vlc.cpp
 *
 *  Created on: 16 de out de 2016
 *      Author: belli
 */



#define PI 3.14159265359

#include <mex.h>
#include <cmath>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_message.h>
#include <gsl/gsl_integration.h>
#include <iostream>
#include <vector>
#include <algorithm>

struct ParamsFunc
{
	double *niveis;
	double *probs;
	double *probsloge;
	int m;
	double sig;
	double sig2;
};

double logeto2;

static double calcInfLog(double y, ParamsFunc *params)
{
	double res = 0;
	int m = params->m;
	for(int i=0;i<m;++i)
	{
		double temp = y-params->niveis[i];
		res+=params->probs[i]*exp(-temp*temp/params->sig2);
	}
	return res;
}

static double calcLog2val(double y, ParamsFunc *params, int j)
{
	double temp = y-params->niveis[j];
	double divisor = calcInfLog(y, params);
	double mult = exp(-temp*temp/(params->sig2));

	double v1 = mult / divisor;
	if(v1 < 1e-100)
	{
		return 0.0;
	}
	if(isnan(v1) || isinf(v1))
	{
		mexPrintf("Erro calcLog2val inf or nan\n");
	}
	return log2(v1);
}

static double calcLog2val_2(double y,  ParamsFunc *params, int j)
{
	double temp1 = y-params->niveis[j];
	temp1=temp1*temp1;
	int m=params->m;
	double res=0;
	for(int i=0;i<m;++i)
	{
		if(i==j)
		{
			res+=params->probs[i];
		}
		else
		{
			double temp = y-params->niveis[i];
			temp=temp*temp;
			double temp3 = params->probs[i]*exp((temp1-temp)/params->sig2);

			res+=temp3;

		}
	}
	if(isinf(res) || isnan(res) || res<0)
	{
		mexPrintf("res=%e ERRO!!!!!!!!!!!!!!!!11\n\n", res);
	}
	//
	double temp5 = -log2(res);

	if(isnan(temp5) || isinf(temp5))
	{
		return 1000;
	}

	return temp5;
}

double calcLog2val_3(double y,  ParamsFunc *params, int j)
{
	double temp1 = y-params->niveis[j];
	temp1=temp1*temp1;
	int m=params->m;

	double max = -1e100;
	std::vector<double> values1(m);
	for(int i=0;i<m;++i)
	{
		if(i==j)
		{
			values1[i] = params->probsloge[i];
		}
		else
		{
			double temp = y-params->niveis[i];
			temp=temp*temp;
			values1[i] = params->probsloge[i]+(temp1-temp)/params->sig2;
		}
		if(max < values1[i])
		{
			max = values1[i];
		}
	}
	double sum1=0;
	for(int i=0;i<m;i++)
	{
		double valt = values1[i]-max;
		if(valt > -36)
		{
			sum1+=exp(valt);
		}
	}

	return (log(sum1)+max)*logeto2;

}

double *levelsAtual;
double *probsAtual;
int nLevelsAtual;
double minIntervalAtual;
double maxInterevalAtual;

void errorgslhandler (const char * reason,
              const char * file,
              int line,
              int gsl_errno)
{
	char temp[5000];
    mexPrintf("nLevels=%d\n", nLevelsAtual);
    
    for(int i=0;i<nLevelsAtual;++i)
    {
        mexPrintf("level=%e - prob=%e - \n", levelsAtual[i], probsAtual[i]);
    }
    
    mexPrintf("Ini=%f - Fim=%f\n", minIntervalAtual, maxInterevalAtual);

	sprintf(temp, "Erro %s - %s - %d - %d\n", reason, file, line, gsl_errno);
	mexErrMsgTxt(temp);
}



static double calc_integrand(double y, void *params_int)
{
	ParamsFunc *params = (ParamsFunc *)params_int;

	int m = params->m;
	double res = 0;
	for(int i=0;i<m;++i)
	{
		double temp = y-params->niveis[i];
		temp=temp*temp/params->sig2;
		if(temp > 100)
		{
			continue;
		}
		double res1 = -calcLog2val_3(y, params, i);

		double temp2 = params->probs[i] * exp(-temp) * res1;
		if(isnan(temp2) || isinf(temp2))
		{
			mexPrintf("temp2 inf - temp=%e - res1=%e\n", temp, res1);
		}
		res += temp2;
	}
    if(isnan(res) || isinf(res))
    {
        mexPrintf("ERRO aqui res=%e\n", res);
    }
	return res;
}

void calcMaxMin(double *levels, int nLevels, double *maxv, double *minv)
{
	*maxv = 0;
	*minv = 1;
	for(int i=0;i<nLevels;++i)
	{
		if(*maxv<levels[i])
		{
			*maxv=levels[i];
		}
		if(*minv>levels[i])
		{
			*minv=levels[i];
		}
	}
}

static void sortLevelsProbs(int nLevels, double *levels, double *probs)
{
	for(int i=0;i<nLevels;++i)
	{
		for(int k=i;k<nLevels;++k)
		{
			if(levels[i]>levels[k])
			{
				double temp = levels[k];
				levels[k]=levels[i];
				levels[i]=temp;
				temp = probs[k];
				probs[k]=probs[i];
				probs[i]=temp;
			}
		}
	}
}


/**
 *
 */
double calcIntegral(double valIni, double valFim, ParamsFunc *params, gsl_integration_workspace *w)
{
	gsl_function func1;
	func1.function = &calc_integrand;

	func1.params = (void *)params;
	double result;
	double error;
	double maxv, minv;

	gsl_integration_qags (&func1, valIni, valFim, 1e-6, 1e-6, 2000,
							w, &result, &error);

	//mexPrintf("result=%e\n", result);
	//result = result/sqrt(params.sig2*PI);
	return result;
}


/**
 * capacity_vlc(levels, probs, sig)
 */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
	if(nrhs != 3 || (nlhs != 1 && nlhs !=0))
	{
		mexErrMsgTxt("Wrong number of parameters: answer = capacity_vlc(levels, probs, sig);");
	}

	int nLevels = mxGetM(prhs[0])*mxGetN(prhs[0]);
	int nProbs = mxGetM(prhs[1])*mxGetN(prhs[1]);
	if(nLevels != nProbs)
	{
		mexErrMsgTxt("Erro, tamanho diferente entre levels e probs: capacity_vlc(levels, probs, sig);");
	}

	double *levels, *probs, *sig;
	levels= mxGetPr(prhs[0]);
	probs = mxGetPr(prhs[1]);
	sig   = mxGetPr(prhs[2]);


    
    levelsAtual=levels;
    probsAtual=probs;
    nLevelsAtual=nLevels;
    
	sortLevelsProbs(nLevels, levels, probs);
    
	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
	double *res = mxGetPr(plhs[0]);

	if(levels[0]<0 || levels[nLevels-1]>2)
	{
		res[0]=0;
		return;
	}

	logeto2 = log2(exp(1));

	//calcula o loge das probabilidades
	std::vector<double> probsE(nProbs);
	for(int i=0;i<nProbs;++i)
	{
		if(probs[i] > 0)
		{
			probsE[i] = log(probs[i]);
		}
		else
		{
			probsE[i] = -2000;
		}
	}


	ParamsFunc params;
	params.m = nLevels;
	params.niveis = levels;
	params.probs  = probs;
	params.sig    = sig[0];
	params.sig2   = params.sig * params.sig * 2;
	params.probsloge = &probsE[0];

	//mexPrintf("m=%d - sig=%f\n", params.m, params.sig);


	gsl_integration_workspace *w = gsl_integration_workspace_alloc (2000);

	gsl_set_error_handler (errorgslhandler);

	double result = 0;

	for(int i=0;i<nLevels;++i)
	{
		double ini, fim;
		ini = levels[i]-8*sig[0];
		fim = levels[i]+8*sig[0];
		if(i!=0)
		{
			double mid = (levels[i-1]+levels[i])/2.0;
			if(ini < mid)
			{
				ini = mid;
			}
		}
		if(i!=nLevels-1)
		{
			double mid = (levels[i+1]+levels[i])/2.0;
			if(fim > mid)
			{
				fim = mid;
			}
		}

		minIntervalAtual=ini;
		maxInterevalAtual=fim;

		result+=calcIntegral(ini, fim, &params, w);
	}
	result /= sqrt(params.sig2*PI);



	res[0]=result;


}
