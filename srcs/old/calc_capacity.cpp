
/*
 * calc_capacity.cpp
 *
 *  Created on: 12 de out de 2016
 *      Author: belli
 */



#define PI 3.14159265359

#include <cmath>
#include <gsl/gsl_integration.h>
#include <iostream>

struct ParamsFunc
{
	double *niveis;
	double *probs;
	int m;
	double sig;
	double sig2;
};

static double calcLog2val(double y, ParamsFunc *params, int j);
static double calcFuncBefInt(double y, ParamsFunc *params);
static double calcInfLog(double y, ParamsFunc *params);
static double int1(double y, void *params);

double calc_capacity_vlc(int m, double* niveis, double* probs, double sig)
{
	ParamsFunc params;
	gsl_integration_workspace *w = gsl_integration_workspace_alloc (2000);
	gsl_function func1;
	func1.function = &int1;

	params.m = m;
	params.niveis = niveis;
	params.probs = probs;
	params.sig = sig;
	params.sig2 = sig*sig*2;

	func1.params = (void *)&params;
	double result;
	double error;
        
        

	gsl_integration_qags (&func1, -8*sig, 1+8*sig, 1e-6, 1e-6, 2000,
	                        w, &result, &error);

	//std::cout << "result = " << result << "  - error = " << error << std::endl;

	return result/sqrt(params.sig2*PI);

}

static double int1(double y, void *params)
{
	ParamsFunc *pp = (ParamsFunc *)params;
	return calcFuncBefInt(y, pp);
}

static double calcFuncBefInt(double y, ParamsFunc *params)
{
	int m = params->m;
	double res = 0;
	for(int i=0;i<m;++i)
	{
		double temp = y-params->niveis[i];
		res += params->probs[i]*exp(-temp*temp/params->sig2)*calcLog2val(y, params, i);
	}
	return res;
}

static double calcLog2val(double y, ParamsFunc *params, int j)
{
	double temp = y-params->niveis[j];
	double v1 = exp(-temp*temp/(params->sig2))/calcInfLog(y, params);
	if(v1 < 1e-30)
	{
		return 0.0;
	}
	return log2(v1);
}


static double calcInfLog(double y, ParamsFunc *params)
{
	double res = 0;
	int m = params->m;
	for(int i=0;i<m;++i)
	{
		double temp = y-params->niveis[i];
		res+=params->probs[i]*exp(-temp*temp/params->sig2);
	}
	return res;
}


