/*
 * testequad.cpp
 *
 *  Created on: 27 de mar de 2017
 *      Author: belli
 */


#include <iostream>
#include "quadpackcpp/quadpackpp-code/include/workspace.hpp"
#include <boost/multiprecision/float128.hpp>

using namespace boost::multiprecision;

float128 getf3(float128 x, void *data)
{
	return x*x*x*x*x;
}

int main(int argc, char *argv[])
{

	quad::Workspace<float128> work(1000, 10);

	quad::Function<float128, void> f(getf3, NULL);
	float128 result, abserror;
	work.qag(f, 0, 1, 1e-20Q, 0, result, abserror);

	std::cout << "Result=" << result << " - " << (result - (1.0Q/6.0Q)) << " - abserror=" << abserror << std::endl;
	return 0;
}
