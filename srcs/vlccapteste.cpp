/*
 * vlccapteste.cpp
 *
 *  Created on: 19 de mar de 2017
 *      Author: belli
 */

#include "probs_vlc.h"

#include <mex.h>



/**
 * rres = vlccapteste(levels, probs, sig)
 */
void mexFunction(int nlhs, mxArray *plhs[],
                 int nrhs, const mxArray *prhs[])
{
	if(nrhs != 3 || (nlhs != 1 && nlhs !=0))
	{
		mexErrMsgTxt("Wrong number of parameters: answer = capacity_vlc(levels, probs, sig);");
	}

	int nLevels = mxGetM(prhs[0])*mxGetN(prhs[0]);
	int nProbs = mxGetM(prhs[1])*mxGetN(prhs[1]);
	if(nLevels != nProbs)
	{
		mexErrMsgTxt("Erro, tamanho diferente entre levels e probs: capacity_vlc(levels, probs, sig);");
	}

	double *levels, *probs, *sig;
	levels= mxGetPr(prhs[0]);
	probs = mxGetPr(prhs[1]);
	sig   = mxGetPr(prhs[2]);

	plhs[0] = mxCreateDoubleMatrix(1, 1, mxREAL);
	double *res = mxGetPr(plhs[0]);

	DataVlc_st data;

	data.levels.resize(nLevels);
	data.probs.resize(nLevels);

	for(int i=0;i<nLevels;++i)
	{
		data.levels[i] = levels[i];
		data.probs[i] = probs[i];

	}
	data.sigma = sig[0];
	data._2xsigma2 = data.sigma*data.sigma*2;
	data.m = nLevels;

	lldouble res1 = calcCapacity1(&data);

	*res = res1;

}


