/*
 * QuadPackInterv.h
 *
 *  Created on: 29 de mar de 2017
 *      Author: belli
 */

#ifndef LIB_QUADPACKINTERV_H_
#define LIB_QUADPACKINTERV_H_



#include "typesInt.h"


namespace quad
{

template<typename Real>
class Workspace;


struct QuadPacIntervkData
{
	std::vector<lldouble> intervIni;
	lldouble abseps;
	lldouble releps;
};

struct ResultData_st
{
	lldouble res;
	lldouble err;
};

struct InfoInterval
{
	lldouble ini;
	lldouble fim;
	lldouble res;
	lldouble err;
};

class QuadPackInterv
{
public:
	QuadPackInterv(int n, int m);
	virtual ~QuadPackInterv();

	void setFunction(FtnBase<lldouble>* f);

	int integrate(lldouble a, lldouble b, const QuadPacIntervkData &data, ResultData_st *res);

private:
	quad::GaussKronrod<lldouble> *gauss;
	std::vector<InfoInterval> intervals;
	int nextInterval;

	ResultData_st result;

	int addInterval(lldouble ini, lldouble fim);
	int splitInterval(int intAtual);
	int splitInterval(int intAtual, lldouble pos);

	FtnBase<lldouble>* f;
};

};

#endif /* LIB_QUADPACKINTERV_H_ */
