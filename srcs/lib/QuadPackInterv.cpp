/*
 * QuadPackInterv.cpp
 *
 *  Created on: 29 de mar de 2017
 *      Author: belli
 */

#include "QuadPackInterv.h"

#include "../quadpackcpp/quadpackpp-code/include/gauss-kronrod.hpp"

#include <boost/multiprecision/float128.hpp>

namespace quad
{

QuadPackInterv::QuadPackInterv(int n, int m)
{
	result.err = 0;
	result.res = 0;
	f = NULL;
	gauss = new GaussKronrod(m);
	nextInterval = 0;
	intervals.resize(n);
}

QuadPackInterv::~QuadPackInterv() {
	// TODO Auto-generated destructor stub
	delete gauss;
}

int QuadPackInterv::integrate(lldouble a, lldouble b,
		const QuadPacIntervkData& data, ResultData_st *res)
{
	nextInterval = 0;
	int n = data.intervIni.size();

	result.err = 0.0Q;
	result.res = 0,0Q;
	lldouble resabs, resasc;

	intervals[nextInterval].ini = a;
	for(int i=0;i<data.intervIni.size();i++)
	{

		intervals[nextInterval].fim = data.intervIni[i];
		gauss->qk(*f, intervals[nextInterval].ini, intervals[nextInterval].fim, intervals[nextInterval].res, intervals[nextInterval].err,
					resabs, resasc);

		result.err += intervals[nextInterval].res;
		result.res += intervals[nextInterval].err;

		nextInterval++;
		intervals[nextInterval].ini = data.intervIni[i];

	}

	intervals[nextInterval].fim = b;
	gauss->qk(*f, intervals[nextInterval].ini, intervals[nextInterval].fim, intervals[nextInterval].res, intervals[nextInterval].err,
				resabs, resasc);

	result.err += intervals[nextInterval].res;
	result.res += intervals[nextInterval].err;

	nextInterval++;

	while(result.err > data.abseps)
	{

	}

	return 0;

}

int QuadPackInterv::addInterval(lldouble ini, lldouble fim)
{
	if(nextInterval >= intervals.size())
	{
		return -1;
	}

	intervals[nextInterval].ini = ini;
	intervals[nextInterval].fim = fim;

	lldouble resabs, resasc;

	gauss->qk(*f, ini, fim, intervals[nextInterval].res, intervals[nextInterval].err,
			resabs, resasc);

	result.err += intervals[nextInterval].err;
	result.res += intervals[nextInterval].res;

	nextInterval++;

	return 0;
}

int QuadPackInterv::splitInterval(int intAtual)
{
	if(intAtual >= nextInterval)
	{
		return -1;
	}

	lldouble ini = intervals[intAtual].ini;
	lldouble fim = intervals[intAtual].fim;

	lldouble valAt = intervals[intAtual].res;
	lldouble errAt = intervals[intAtual].err;

	intervals[intAtual].fim = (ini + fim)/2;
	lldouble resabs, resasc;
	gauss->qk(*f, intervals[intAtual].ini, intervals[intAtual].fim, intervals[intAtual].res, intervals[intAtual].err,
				resabs, resasc);

	result.err += intervals[intAtual].err - errAt;
	result.res += intervals[intAtual].res - valAt;

	addInterval((ini + fim)/2, fim);

	return 0;
}

void QuadPackInterv::setFunction(FtnBase<lldouble>* f)
{
	this->f = f;
}

int QuadPackInterv::splitInterval(int intAtual, lldouble pos)
{
}

};
