/*
 * probs_vlc_128.h
 *
 *  Created on: 26 de mar de 2017
 *      Author: belli
 */

#ifndef LIB_PROBS_VLC_128_H_
#define LIB_PROBS_VLC_128_H_

#include <boost/multiprecision/float128.hpp>
#include <vector>


using lldouble = boost::multiprecision::float128;

struct DataVlc_st
{
	lldouble sigma;
	lldouble _2xsigma2;
	lldouble log_v_2;
	std::vector<lldouble> levels; // níveis
	std::vector<lldouble> probs;	 // probabilidades
	lldouble log_val_2;

	int m; // número de níveis M-PAM
};

lldouble calcCapacity128(DataVlc_st *data);


#endif /* LIB_PROBS_VLC_128_H_ */
