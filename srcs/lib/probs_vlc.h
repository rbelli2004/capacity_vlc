/*
 * probs_vlc.h
 *
 *  Created on: 17 de mar de 2017
 *      Author: belli
 */

#ifndef LIB_PROBS_VLC_H_
#define LIB_PROBS_VLC_H_

#include <vector>

#include "typesInt.h"

struct DataVlc_st
{
	lldouble sigma;
	lldouble _2xsigma2;
	std::vector<lldouble> levels; // níveis
	std::vector<lldouble> probs;	 // probabilidades

	int m; // número de níveis M-PAM
};

lldouble calcCapacity1(DataVlc_st *data);



#endif /* LIB_PROBS_VLC_H_ */
