/*
 * typesInt.h
 *
 *  Created on: 29 de mar de 2017
 *      Author: belli
 */

#ifndef LIB_TYPESINT_H_
#define LIB_TYPESINT_H_

#include <boost/multiprecision/float128.hpp>



typedef boost::multiprecision::float128 lldouble; // neste caso funciona somente para double...

#endif /* LIB_TYPESINT_H_ */

