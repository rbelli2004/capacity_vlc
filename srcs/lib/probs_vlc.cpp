/*
 * probs_vlc.cpp
 *
 *  Created on: 17 de mar de 2017
 *      Author: belli
 */

#include "probs_vlc.h"

#include <cmath>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_message.h>
#include <mex.h>
//#include <boost/numeric/odeint.hpp>

constexpr lldouble PI = 3.141592653589793238462643383279502884197;

/*
using state_type=ldouble;

class CalcCapacityClass
{
public:
	CalcCapacityClass(DataVlc_st *data);
	void operator()(const state_type &y, state_type &dydx, const ldouble x);
private:
	DataVlc_st datav;
};

inline CalcCapacityClass::CalcCapacityClass(DataVlc_st* data)
{
	datav = *data;
}

inline void CalcCapacityClass::operator ()(const state_type& y,
		state_type& dydx, const ldouble x)
{
	y = calcFy2(x, &datav);
}
*/

static lldouble calcIntegral(gsl_integration_workspace * w, gsl_function *F, lldouble ini, lldouble fim);
static lldouble calcFy1(lldouble y, DataVlc_st *data);

void errorgslhandler (const char * reason,
              const char * file,
              int line,
              int gsl_errno)
{
	char temp[5000];


	sprintf(temp, "Erro %s - %s - %d - %d\n", reason, file, line, gsl_errno);
	mexErrMsgTxt(temp);
}


static lldouble calcFy2(lldouble y, DataVlc_st *data)
{
	lldouble v1 = calcFy1(y, data);

	if(v1 < 1e-100)
	{
		return 0;
	}

	v1 = -v1*log2(v1);
	return v1;
}

static lldouble calcFy1(lldouble y, DataVlc_st *data)
{
	/*ldouble min = 1e10;
	ldouble dist;
	for(int i=0;i<data->m;++i)
	{
		dist = (y-data->levels[i]);
		dist=dist*dist;
		if(dist < min)
		{
			min = dist;
		}
		data->diffs[i] = dist;
	}*/
	lldouble res = 0;
	for(int i=0;i<data->m;++i)
	{
		lldouble expv;
		expv = (y - data->levels[i]);
		expv = exp(-(expv*expv) / data->_2xsigma2);

		res += data->probs[i] * expv;
	}
	res*= 1/sqrt(data->_2xsigma2 * PI);

	return res;
}

static double calcStep(double x, void *param)
{
	DataVlc_st *v1 = (DataVlc_st *)param;
	return calcFy2(x, v1);
}

lldouble calcCapacity1(DataVlc_st *data)
{
	gsl_integration_workspace * w
	    = gsl_integration_workspace_alloc (3000);

	gsl_set_error_handler (errorgslhandler);

	gsl_function F;
	
	F.function = &calcStep;
	F.params = data;

	lldouble res = 0;


	for(int l=0;l<=data->m;++l)
	{
		lldouble ini;
		lldouble fim;
		if(l==0)
		{
			ini = data->levels[0] - 20*data->sigma;
			fim = data->levels[0];
		}
		else if(l== (data->m))
		{
			ini = data->levels[data->m-1];
			fim = data->levels[data->m-1]+20*data->sigma;
		}
		else
		{
			ini = data->levels[l-1];
			fim = data->levels[l];
		}
		res+=calcIntegral(w, &F, ini, fim);
	}

	res = res - log2(data->sigma * sqrt(2*PI*exp(1.0)));

	return res;

}

static lldouble calcIntegral(gsl_integration_workspace * w, gsl_function *F, lldouble ini, lldouble fim)
{
	lldouble result;
	lldouble error;


	int ret = gsl_integration_qags (F, ini, fim, 0, 1e-9, 3000,
	                        w, &result, &error);

	return result;
}


