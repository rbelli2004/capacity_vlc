/*
 * probs_vlc_128.cpp
 *
 *  Created on: 26 de mar de 2017
 *      Author: belli
 */

#include "probs_vlc_128.h"

#include "../quadpackcpp/quadpackpp-code/include/workspace.hpp"

using namespace boost::multiprecision;

using namespace quad;

constexpr lldouble pi = 3.1415926535897932384626433832795028841971693993751058Q;


static lldouble calcFy1(lldouble y, DataVlc_st *data);

void errorgslhandler (const char * reason,
              const char * file,
              int line,
              int gsl_errno)
{
	char temp[5000];


	sprintf(temp, "Erro %s - %s - %d - %d\n", reason, file, line, gsl_errno);
	mexErrMsgTxt(temp);
}


static lldouble calcFy2(lldouble y, DataVlc_st *data)
{
	lldouble v1 = calcFy1(y, data);

	if(v1 < 1e-300Q)
	{
		return 0;
	}

	v1 = -v1*boost::multiprecision::log(v1) / data->log_val_2;
	return v1;
}

static lldouble calcFy1(lldouble y, DataVlc_st *data)
{
	/*ldouble min = 1e10;
	ldouble dist;
	for(int i=0;i<data->m;++i)
	{
		dist = (y-data->levels[i]);
		dist=dist*dist;
		if(dist < min)
		{
			min = dist;
		}
		data->diffs[i] = dist;
	}*/
	lldouble res = 0;
	for(int i=0;i<data->m;++i)
	{
		lldouble expv;
		expv = (y - data->levels[i]);
		expv = boost::multiprecision::exp(-(expv*expv) / data->_2xsigma2);

		res += data->probs[i] * expv;
	}
	res*= 1/boost::multiprecision::sqrt(data->_2xsigma2 * PI);

	return res;
}

static lldouble calcStep(lldouble x, DataVlc_st *param)
{

	return calcFy2(x, param);
}


lldouble calcCapacity128(DataVlc_st *data)
{
	int limit = 3000;
	size_t m_deg = 10;
	Workspace<lldouble> work(limit, m_deg);
	data->log_v_2 = boost::multiprecision::log(2.0Q);

	Function<lldouble, DataVlc_st> F(calcFy2, data);

	lldouble result;
	lldouble abserr;
	lldouble resultf = 0Q;
	try
	{
		for(int l=0;l<=data->m;++l)
		{
			lldouble ini;
			lldouble fim;
			if(l==0)
			{
				ini = data->levels[0] - 20*data->sigma;
				fim = data->levels[0];
			}
			else if(l== (data->m))
			{
				ini = data->levels[data->m-1];
				fim = data->levels[data->m-1]+20*data->sigma;
			}
			else
			{
				ini = data->levels[l-1];
				fim = data->levels[l];
			}
			work.qag(F, ini, fim, 1e-18Q, 0Q, result, abserr);
			resultf+=result;
		}
	}
	catch (const char* reason)
	{
		std::cerr << reason << std::endl;
		return -work.get_xmin();
	}
	return resultf;
}

